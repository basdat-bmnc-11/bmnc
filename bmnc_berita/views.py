from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse, HttpResponseBadRequest
from django.db import connection
from django.views.decorators.csrf import csrf_exempt

from bmnc_core.models import Berita, Universitas
from .forms import BeritaForm

import datetime

response = {}

# Create your views here.
def index(request):
    return render(request, 'landing_page.html', response)

def list_berita(request):
    return render(request, 'landing_page.html', response)

def create_berita(request):
    response['news_form'] = BeritaForm
    return render(request, 'tambah_berita.html', response)

@csrf_exempt
def add_berita(request):
    form = BeritaForm(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        url = request.POST['url']
        judul = request.POST['judul']
        topik = request.POST['topik']
        date = str(datetime.datetime.now())
        created_at = date
        updated_at = date
        id_universitas = request.POST['id_universitas']

        query = "INSERT INTO BERITA VALUES ('" + url + "','" + judul + "', '" + topik + "', '" \
                + created_at + "', '" + updated_at + "', " + "0" + ", " + "0" + ", " + id_universitas + ")"
        with connection.cursor() as c:
            c.execute(query)
        return HttpResponseRedirect('/berita/new/')
    return HttpResponseBadRequest()
