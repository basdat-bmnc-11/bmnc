from django.forms import ModelForm, widgets
from bmnc_core.models import Berita

class BeritaForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BeritaForm, self).__init__(*args, **kwargs)
        for name, field in self.fields.items():
            if field.widget.__class__ == widgets.TextInput:
                if 'class' in field.widget.attrs:
                    field.widget.attrs['class'] += 'form-control'
                else:
                    field.widget.attrs.update({'class':'form-control'})
            elif field.widget.__class__ == widgets.Select:
                if 'class' in field.widget.attrs:
                    field.widget.attrs['class'] += 'form-control'
                else:
                    field.widget.attrs.update({'class':'form-control'})

    class Meta:
        model = Berita
        fields = ['url', 'judul', 'topik', 'id_universitas']
