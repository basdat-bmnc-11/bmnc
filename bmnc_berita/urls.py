from django.conf.urls import url
from django.views.generic import RedirectView
from .views import index, list_berita, create_berita, add_berita

urlpatterns = [
    url(r'^$', index, name="index"),
    url(r'^list/$', list_berita, name='list-berita'),

    # Tambah berita
    url(r'^new/$', create_berita, name="create-berita"),
    url(r'^add-berita', add_berita, name="add-berita"),
]
