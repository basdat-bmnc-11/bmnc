from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse, HttpResponseBadRequest
from django.db import connection
from django.views.decorators.csrf import csrf_exempt

from bmnc_core.models import Berita, Polling, PollingBerita, PollingBiasa, Respon
from bmnc_berita.forms import BeritaForm
from .forms import PollingBiasaForm, PollingBeritaForm

import datetime

response = {}

# Create your views here.
def index(request):
    return render(request, 'landing_page.html', response)

def create_polling_biasa(request):
    response['polling_biasa_form'] = PollingBiasaForm
    return render(request, 'tambah_polling_biasa.html', response)

@csrf_exempt
def add_polling_biasa(request):
    return None

def create_polling_berita(request):
    response['polling_berita_form'] = PollingBeritaForm
    return render(request, 'tambah_polling_berita.html', response)

@csrf_exempt
def add_polling_berita(request):
    return None