from django.apps import AppConfig


class BmncPollingConfig(AppConfig):
    name = 'bmnc_polling'
