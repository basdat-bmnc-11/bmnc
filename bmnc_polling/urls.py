from django.conf.urls import url
from django.views.generic import RedirectView
from .views import index, create_polling_biasa, create_polling_berita, add_polling_biasa ,add_polling_berita

urlpatterns = [
    url(r'^$', index, name="index"),

    # Tambah polling

    ## Tambah polling biasa
    url(r'^biasa/new/$', create_polling_biasa, name="create-polling-biasa"),
    url(r'^biasa/add-polling', add_polling_biasa, name="add-polling-biasa"),

    ## Tambah polling berita
    url(r'^berita/new/$', create_polling_berita, name="create-polling-berita"),
    url(r'^berita/add-polling', add_polling_berita, name="add-polling-berita"),
]
