from django.forms import ModelForm, widgets
from bmnc_core.models import Polling, PollingBiasa, PollingBerita

class PollingBiasaForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(PollingBiasaForm, self).__init__(*args, **kwargs)
        for name, field in self.fields.items():
            if field.widget.__class__ == widgets.TextInput:
                if 'class' in field.widget.attrs:
                    field.widget.attrs['class'] += 'form-control'
                else:
                    field.widget.attrs.update({'class':'form-control'})
    
    class Meta:
        model = PollingBiasa
        fields = ['url', 'deskripsi']

class PollingBeritaForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(PollingBeritaForm, self).__init__(*args, **kwargs)
        for name, field in self.fields.items():
            if field.widget.__class__ == widgets.TextInput:
                if 'class' in field.widget.attrs:
                    field.widget.attrs['class'] += 'form-control'
                else:
                    field.widget.attrs.update({'class':'form-control'})
    
    class Meta:
        model = PollingBerita
        fields = ['url_berita']