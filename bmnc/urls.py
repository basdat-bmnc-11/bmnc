"""bmnc URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic import RedirectView
import bmnc_berita.urls as berita
import bmnc_core.urls as home
import app_profile.urls as profile
import app_melihat_daftar_berita.urls as lihat_daftar_berita
import app_melihat_polling_biasa.urls as lihat_polling_biasa
import bmnc_polling.urls as polling
import accounts.urls as account
from bmnc import views

urlpatterns = [
    url(r'^$', views.login_redirect, name='login_redirect'),
    url(r'^admin/', admin.site.urls),
    url(r'^$', RedirectView.as_view(url='home/', permanent=True), name='root'),
    url(r'home/', include(home, namespace='home')),
    url(r'daftar_berita/', include(lihat_daftar_berita, namespace='lihat_daftar_berita')),
    url(r'daftar_polling_biasa/', include(lihat_polling_biasa, namespace='lihat_polling_biasa')),
    url(r'profile/', include(profile, namespace='profile')),
    url(r'berita/', include(berita, namespace='berita')),
    # url(r'^$', RedirectView.as_view(url='home/', permanent=True), name='root'),
    # url(r'home/', include(home, namespace='home')),
    url(r'^account/', include(account, namespace='login page')),
    url(r'^polling/', include(polling, namespace='polling')),
]
