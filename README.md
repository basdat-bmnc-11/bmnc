# Aplikasi BMNC

Tugas Kelompok Studi Kasus BMNC

Basis Data FASILKOM UI, Semester Genap 2017/2018

Repositori _git_ ini berisi kode sumber untuk aplikasi _BMNC_,
yang dibuat oleh kelompok 11 Kelas B  yang beranggotakan: 
- Muhammad Rezki (1606875812)
- Muhammad Yudistira Hanifmuti (1606829560)
- Muhammad Sulthan Rafi Shaquille (1606875882)
  
## Status

Berikut adalah informasi detil tentang _branch_ utama:

[![pipeline status](https://gitlab.com/basdat-bmnc-11/bmnc/badges/master/pipeline.svg)](https://gitlab.com/basdat-bmnc-11/bmnc/commits/master)

[![coverage report](https://gitlab.com/basdat-bmnc-11/bmnc/badges/master/coverage.svg)](https://gitlab.com/basdat-bmnc-11/bmnc/commits/master)
  
## Catatan Untuk Pengembang (Anggota Kelompok)

### Memulai untuk pertama kalinya

**Cobalah untuk me-_refresh_ pengetahuan Anda tentang _Django_ dari repositori praktikum PPW Anda.**
Versi dari _Django_ yang digunakan sama persis dengan versi yang digunakan pada praktikum (1.11.4). Disediakan juga
_Selenium_ bagi yang ingin melakukan _functional test_.

### Setup Local Postgres

**Lakukan hal berikut hanya jika pada localhost anda belum terdapat skema BMNC.**

- Buka psql shell pada komputer anda (kalau belum punya bisa download dan diikuti langkah instalasinya di [sini](http://www.postgresqltutorial.com/install-postgresql/))
- Setelah masuk ke database anda, buat schema baru bernama BMNC.

    ```sql
    CREATE SCHEMA bmnc;
    ```

- Buat tabel sesuai dengan deskripsi tabel pada TK 3 (bisa minta Rezki)

**Jika sudah ada skema BMNC pada localhost, anda dapat langsung memulai dari langkah berikut.**

- Buka file `bmnc/settings.py` dan perhatikan `line 80 - 92`
- Ubah pengaturan sesuai dengan localhost anda,
    
    ```python
    DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': '[dbname]',
        'USER': 'postgres',
        'PASSWORD': '[password]',
        'HOST': 'localhost',
        'PORT': '5432',
        'OPTIONS' : {
            'options': '-c search_path=bmnc'
            }
        }
    }
    ```

- Coba jalankan `manage.py runserver`. Jika sudah benar, maka seharusnya `runserver` dapat dijalankan.

### Menambahkan apps baru

- Membuat branch baru sesuai dengan nama aplikasi. Gunakan perintah Git, misal `git checkout -b <app-name> master`

- ``` python manage.py startapp <app-name> ```

    > ganti `<app-name>` menggunakan nama sesuai kebutuhan/keinginan kalian, misalkan `bmnc_x`. 
    > sebelum menjalankan perintah ini, pastikan sudah berada satu direktori dengan berkas `manage.py`.

- Buat folder `<app-name>/templates` dan `<app-name>/static`. Folder tersebut akan menjadi tempat anda menyimpan semua file untuk tampilan (`html`, `css` dan `js`).

- Untuk membuat suatu file `html` cukup melakukan _extends_ dari `base.html`.
    ```html
    {% extends "layout/base.html" %}

    {% block title %}
        <!-- insert title here -->
    {% endblock %}

    {% block style %}
        <!-- insert static/css here -->
    {% endblock %}

    {% block staticfiles %}
        <!-- insert static files here -->
    {% endblock %}

    {% block content %}
        <h1>Contoh content</h1>
        <h2>Cukup mengisi bagian sini saja</h2>
    {% endblock %}

    {% block javascript %}
        <!-- insert script here -->
    {% endblock %}
    ```

- Lakukan _commit_ secara berkala dengan perintah `git commit`

- Jika dirasa sudah cukup mengimplementasikan fitur anda, lakukan Git push. Gunakan perintah Git, misal `git push -u origin <branch-name>`