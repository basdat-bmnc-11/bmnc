from django.contrib import admin
from .models import Berita, Narasumber, Universitas

admin.site.register(Berita)
admin.site.register(Narasumber)
admin.site.register(Universitas)