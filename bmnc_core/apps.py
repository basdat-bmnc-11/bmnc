from django.apps import AppConfig


class BmncCoreConfig(AppConfig):
    name = 'bmnc_core'
